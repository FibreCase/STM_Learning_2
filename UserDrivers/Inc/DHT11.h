//
// Created by fibre on 2024/1/22.
//

#ifndef DHT11_H
#define DHT11_H

#include "main.h"

uint8_t DHT11_Read_Humi_Temp(uint8_t *humi_h,uint8_t *humi_l,uint8_t *temp_h,uint8_t *temp_l);

#endif //DHT11_H
