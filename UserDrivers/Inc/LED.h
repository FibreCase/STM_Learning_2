//
// Created by fibre on 2024/1/22.
//

#ifndef LED_H
#define LED_H

#include "main.h"

void LED_Twinkling(uint16_t GPIO_Pin, uint8_t time);
void LED_Switch(uint16_t GPIO_Pin, uint8_t status);

#endif //LED_H
