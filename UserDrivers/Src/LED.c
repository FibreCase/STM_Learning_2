//
// Created by fibre on 2024/1/22.
//
#include "LED.h"

void LED_Twinkling(uint16_t GPIO_Pin, uint8_t time) {
		HAL_GPIO_WritePin(GPIOA, GPIO_Pin, GPIO_PIN_SET);
		HAL_Delay(time);
		HAL_GPIO_WritePin(GPIOA, GPIO_Pin, GPIO_PIN_RESET);
}

void LED_Switch(uint16_t GPIO_Pin, uint8_t status) {
	if (status) {
		HAL_GPIO_WritePin(GPIOA, GPIO_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(GPIOA, GPIO_Pin, GPIO_PIN_RESET);
	}
}
