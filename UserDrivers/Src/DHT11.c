//
// Created by fibre on 2024/1/22.
//

#include "DHT11.h"

uint16_t time = 0;

#define DHT11_HIGH HAL_GPIO_WritePin(DHT11_GPIO_Port, DHT11_Pin, GPIO_PIN_SET)
#define DHT11_LOW HAL_GPIO_WritePin(DHT11_GPIO_Port, DHT11_Pin, GPIO_PIN_RESET)

void DHT11_Write_Out_Input(uint8_t cmd) //这里和iic那个部分差不多
{
	GPIO_InitTypeDef GPIO_InitStruct;

	if(cmd) //为1的时候是输出模式
	{
		  GPIO_InitStruct.Pin = DHT11_Pin;
		  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		  HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStruct);
	}
	else // 为0的时候上拉输入
	{
		  GPIO_InitStruct.Pin = DHT11_Pin;
		  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
		  HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStruct);
	}
}
uint8_t DHT11_Read_Byte(void)
{
	uint8_t data = 0;
	DHT11_Write_Out_Input(0);
	for(uint8_t i = 0;i<8;i++)
	{
		while((HAL_GPIO_ReadPin(DHT11_GPIO_Port, DHT11_Pin) == GPIO_PIN_RESET)&&(++time<1000));
		time = 0;

		data <<= 1;
		HAL_Delay(40);
		if(HAL_GPIO_ReadPin(DHT11_GPIO_Port, DHT11_Pin) == GPIO_PIN_SET)
		{
			data |= 0x01;
			while((HAL_GPIO_ReadPin(DHT11_GPIO_Port, DHT11_Pin) == GPIO_PIN_SET)&&(++time<1000));
		}
	}
	return data;
}

uint8_t DHT11_Read_Humi_Temp(uint8_t *humi_h,uint8_t *humi_l,uint8_t *temp_h,uint8_t *temp_l)
{
	uint8_t data[5]; //四十位数据

	DHT11_Write_Out_Input(1);
	DHT11_LOW;
	HAL_Delay(20);
	DHT11_HIGH;
	HAL_Delay(30);

	DHT11_Write_Out_Input(0);
	while((HAL_GPIO_ReadPin(DHT11_GPIO_Port, DHT11_Pin) == GPIO_PIN_RESET) && (++time<1000));
	time = 0;
	while((HAL_GPIO_ReadPin(DHT11_GPIO_Port, DHT11_Pin) == GPIO_PIN_SET) && (++time<1000));
	time = 0;

	for(uint8_t i=0;i<5;i++)
	{
		data[i] = DHT11_Read_Byte();
	}
	HAL_Delay(1);
	if(data[0]+data[1]+data[2]+data[3] == data[4])
	{
		*humi_h = data[0];
		*humi_l = data[1];
		*temp_h = data[2];
		*temp_l = data[3];
	}
	else
		return 1;
	return 0;
}
