//
// Created by fibre on 2024/1/22.
//
#include "MH-FMD.h"

void bee(int time) {
	HAL_GPIO_WritePin(GPIOA, BEE_Pin, GPIO_PIN_RESET);
	HAL_Delay(time);
	HAL_GPIO_WritePin(GPIOA, BEE_Pin, GPIO_PIN_SET);
}