//
// Created by fibre on 2024/1/22.
//

#include "EC11.h"

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (enc == 0 || enc==255) {
		OLED_Clear();
	}
	if (htim == &htim2) {
		val = (uint32_t)(__HAL_TIM_IS_TIM_COUNTING_DOWN(&htim2));
		if (val == 1) {
			LED_Switch(LEDG_Pin,1);
			enc++;
			sprintf(encc,"%d",enc);
			OLED_Show_String(0,0,encc);
		}
		if (val == 0) {
			LED_Switch(LEDR_Pin,1);
			enc--;
			sprintf(encc,"%d",enc);
			OLED_Show_String(0,0,encc);
		}
	}

	HAL_Delay(30);
	LED_Switch(LEDR_Pin|LEDG_Pin,0);
}